'use strict';

// TODO: Remove the need for these global variable
var challenges;
var i = 0;
var oppulu = 0;

const $holder = d3.select('ul');
const $next = d3.select('button')
	  .on('click', update)
;


d3.csv("data.csv", row)
	.then(function(data) {
		challenges = d3.shuffle(data);
		update();
	})
// TODO: Show error to the user
	.catch(error => console.error('ERROR: Cannot fetch data:', error))
;

function update() {

	if (i == challenges.length) {
		//challenges = d3.shuffle(challenges);
		//i = 0;
		showFinalScoreCard();
		return;
	}
	// clear prev options
	$holder.selectAll('li').remove();
	$next.style('display', 'none');

	const $options = $holder.selectAll('li').data(d3.shuffle(challenges[i]));

	// ENTER + UPDATE
	$options.enter().append('li')
		.attr('class', '')
		.text(d => d[1])
		.on('click', validate)
		.transition(d3.transition().duration(750))
		.attr('color', 'red')
	;
}

function row(d) {
	return Object.entries(d).filter(d => d[1]);
}


function validate() {

	const n = d3.select(this);

	if (n.classed('oppu') || n.classed('tappu')) {
		update();

	} else {
		n.attr('class', 'selected');
		i = ++i;
		reveal();
	}
}

function reveal() {
	$next
		.style('display', 'block')
	;
	d3.selectAll('li')
		.classed('oppu', d => isCorrect(d))
		.classed('tappu', d => !isCorrect(d))
	;

	const $score = d3.select('#score');
	$score.select('span').text(' /' + i);

	if (!d3.select('li.selected.oppu').empty()) {
		oppulu = ++oppulu;
		$score.select('strong').text(oppulu);
	}
}

function isCorrect(d) {
	return d[0] == 'ఒప్పు';
}

function showFinalScoreCard() {
	
	const margin = {top: 6, right: 6, bottom: 6, left: 6,};
	const $container = d3.select('#scorecard').style('display', 'block');
	const width = $container.node().clientWidth - margin.right - margin.left;
	const height = $container.node().clientHeight - margin.top - margin.bottom;
	const radius = Math.min(width, height) *.72 * .5;

	const $g = $container.append('svg')
		  .attr('width', (radius * 2) + margin.left + margin.right)
		  .attr('height', (radius * 2) + margin.top + margin.bottom)
		  .append('g')
		  .attr('transform', 'translate('
				+ (margin.left + radius) + ','
				+ (margin.top + radius) + ')')
	;

	const oppulaShaatam = oppulu/i;

	const arc = d3.arc()
		  .innerRadius(radius * .75)
		  .outerRadius(radius * .9)
		  .cornerRadius(6)
		  .startAngle(0)
		  .padAngle(.02)
	;

	$g.append('path')
		.attr('d', arc.endAngle(Math.PI * 2 * oppulaShaatam))
		.attr('fill', '#000')
		.transition(d3.transition().duration(750))
		.attr('fill', '#66bd63')
	;
	$g.append('path')
		.attr('d', arc.startAngle(Math.PI * 2 * oppulaShaatam).endAngle(Math.PI * 2))
		.attr('fill', '#f46d43')


	$g.append('text')
		.text(oppulaShaatam === 1 ? '💯' : parseInt(oppulaShaatam * 100, 10) + '%')
		.attr('fill', '#333')
		.attr('font-size', oppulaShaatam === 1 ? '7.5em' : '4em')
		.attr('text-anchor', 'middle')
		.attr('dy', '.3em')
	;

	let text;
	
	if (oppulu === 0) {
		text = 'అన్నీ తప్పులే! మీకో గుండు సున్న!! 😦';
	} else if (oppulaShaatam <= .4) {
		text = 'అన్ని తప్పులా! మీరు చాలా నేర్చుకోవాలి!!';
	} else if (oppulaShaatam < .75) {
		text = 'పర్లేదు, కానీ మీరు ఇంకా మెరుగవ్వాలి!';
	} else if (oppulaShaatam < .9) {
		text = 'అబ్బో, పర్లేదే! ఇంకొంచెం శ్రద్ధ పెట్టండి.';
	} else if (oppulaShaatam < 1) {
		text = 'అభినందనలు! ఎక్కడో పప్పులో కాలేసారు. 👍';
	} else {
		text = 'అద్భుతం! మీకు తిరుగులేదు!! 👍'
	}
	
	$container.append('p')
		.text(text)
	;

	$container.append('button')
		.text('మళ్ళీ ఆడండి!')
		.on('click', replay)
	;
}

function replay() {
	challenges = d3.shuffle(challenges);
	i = 0;
	oppulu = 0;

	d3.select('#scorecard').style('display', 'none').html('');
	d3.select('#score strong').text('0');
	d3.select('#score span').text('0');

	update();
	
}
